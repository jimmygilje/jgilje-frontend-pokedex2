/** @jsx jsx */
import { jsx } from "@emotion/core";
import { Box } from "./Utilities";

const DetailSectionHeader = props => (
  <Box
    bg="#4e988f"
    fontSize={17}
    fontWeight="600"
    width="100%"
    pl={4}
    py={1}
    color="white"
  >
    {props.children}
  </Box>
);

export default DetailSectionHeader;
