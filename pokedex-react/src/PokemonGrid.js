import React from "react";
import axios from "axios";
import PokemonCard from "./PokemonCard";
import { Box, Grid, Flex } from "@chakra-ui/core";
import Search from "./Search";
import ArrowButton from "./ArrowButton";
import { withRouter } from "react-router-dom";
import queryString from "query-string";

//using Chakra for main grid, also learned to use basic styled system and emotion in other components

class PokemonGrid extends React.Component {
  state = {
    currentPage: 1,
    pokemonData: undefined,
    isLoaded: false,
    searchText: null
  };

  componentDidMount() {
    this.backListener = this.props.history.listen(this.handlePopState);

    //update state from query params
    const values = queryString.parse(this.props.location.search);

    if (values) {
      var newState = {};
      if (values.searchText) newState.searchText = values.searchText;
      if (values.page) newState.currentPage = values.page;
      this.setState(newState, this.refresh);
    } else {
      this.refresh();
    }
  }

  refresh() {
    if (this.state.searchText || this.state.currentPage) {
      var newQuery = {};
      if (this.state.searchText) newQuery.name = this.state.searchText;
      if (this.state.currentPage) newQuery.page = this.state.currentPage;
    }
    axios
      .get(
        `https://intern-pokedex.myriadapps.com/api/v1/pokemon/?${queryString.stringify(
          newQuery
        )}`
      )
      .then(response => {
        this.setState({
          pokemonData: response.data.data,
          isLoaded: true,
          currentPage: response.data.meta.current_page
        });
      });
  }

  onSearchSubmit = event => {
    event.preventDefault();
    var searchValue = event.target.elements.searchInput.value;
    this.setState({ searchText: searchValue, currentPage: 1 }, function() {
      this.props.history.push(`?searchText=${searchValue}`);
      this.refresh();
    });
  };

  prevPage = () => {
    const values = queryString.parse(this.props.location.search);
    values.page = this.state.currentPage - 1;
    this.setState(
      prevState => ({ currentPage: prevState.currentPage - 1 }),
      function() {
        this.props.history.push(`?${queryString.stringify(values)}`);
        this.refresh();
      }
    );
  };

  nextPage = () => {
    const values = queryString.parse(this.props.location.search);
    values.page = this.state.currentPage + 1;
    this.setState(
      prevState => ({ currentPage: prevState.currentPage + 1 }),
      function() {
        this.props.history.push(`?${queryString.stringify(values)}`);
        this.refresh();
      }
    );
  };

  componentWillUnmount() {
    //stop listening
    this.backListener();
  }

  handlePopState = (location, action) => {
    if (action === "POP") {
      const values = queryString.parse(this.props.location.search);
      var newState = { searchText: null, currentPage: null };
      if (values.searchText) newState.searchText = values.searchText;
      if (values.page) newState.currentPage = values.page;
      this.setState(newState, this.refresh);
    }
  };

  render() {
    var shouldBeActive = false;
    if (this.state.currentPage > 1) shouldBeActive = true;
    return (
      <Box>
        <Flex mb={4}>
          <ArrowButton
            isLeft
            isActive={shouldBeActive}
            callbackFunc={this.prevPage}
          />
          <Search onSearchSubmit={this.onSearchSubmit}></Search>
          <ArrowButton isActive={true} callbackFunc={this.nextPage} />
        </Flex>

        <Grid
          templateColumns="repeat(auto-fill, 220px)"
          gap="3"
          justifyContent="center"
          alignItems="stretch"
          m="0 16px"
        >
          {this.state.isLoaded
            ? this.state.pokemonData.map(pokemon => (
                <PokemonCard key={pokemon.id} pokemon={pokemon} />
              ))
            : "Loading..."}
        </Grid>
      </Box>
    );
  }
}

export default withRouter(PokemonGrid);
