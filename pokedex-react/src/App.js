import React from "react";
import PokemonDetail from "./PokemonDetail";
import PokemonGrid from "./PokemonGrid";
import { Box, ThemeProvider, CSSReset } from "@chakra-ui/core";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <ThemeProvider>
        <CSSReset />
        <Box w="100%" bg="#55a69c" minHeight="100vh">
          <Box maxW="1000px" m="0px auto" py={4}>
            <Switch>
              <Route
                path="/detail/:id"
                render={props => <PokemonDetail {...props} />}
              />
              <Route path="/">
                <PokemonGrid />
              </Route>
            </Switch>
          </Box>
        </Box>
      </ThemeProvider>
    </Router>
  );
}

export default App;
