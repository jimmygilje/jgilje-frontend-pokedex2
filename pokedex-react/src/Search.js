import React from "react";
import { InputGroup, InputLeftElement, Input, Icon } from "@chakra-ui/core";

class Search extends React.Component {
  render() {
    return (
      <InputGroup
        bg="#519f95"
        color="#fff"
        flex={3}
        size="lg"
        alignItems="center"
        mx={4}
        my={3}
        borderRadius="5px"
        _placeholder={{ color: "red" }}
      >
        <InputLeftElement children={<Icon name="search" />} top="unset" />
        <form onSubmit={this.props.onSearchSubmit}>
          <Input
            name="searchInput"
            placeholder="Pokedex"
            bg="#519f95"
            fontSize="68px"
            variant="filled"
            p={10}
            focusBorderColor="none"
            borderRadius="5px"
            _placeholder={{ color: "#458880" }}
            fontWeight="700"
          />
        </form>
      </InputGroup>
    );
  }
}

export default Search;
