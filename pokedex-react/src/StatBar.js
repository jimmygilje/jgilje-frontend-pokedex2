/** @jsx jsx */
import { jsx } from "@emotion/core";
import { Box, Row } from "./Utilities";

const StatBar = function(props) {
  var statPercent = (props.stat[1] / 250) * 100;
  return (
    <Row my={1} alignItems="flex-start">
      <Box flex={1} fontSize={15}>
        {props.stat[0]}
      </Box>
      <Box bg="#4e988f5c" fontSize={16} flex={2} color="white">
        <Box bg="#4e988f" width={`${statPercent}%`} pl={2}>
          {props.stat[1]}
        </Box>
      </Box>
    </Row>
  );
};

export default StatBar;
