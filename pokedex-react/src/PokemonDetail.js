/** @jsx jsx */
import React from "react";
import axios from "axios";
import { jsx } from "@emotion/core";
import { Box, Row } from "./Utilities";
import DetailSectionHeader from "./DetailSectionHeader";
import StatBar from "./StatBar";
import TypeDisplay from "./TypeDisplay";
import ArrowButton from "./ArrowButton";
import { withRouter } from "react-router-dom";

class PokemonDetail extends React.Component {
  state = { pokemon: {} };
  componentDidMount() {
    axios
      .get(
        `https://intern-pokedex.myriadapps.com/api/v1/pokemon/${this.props.match.params.id}`
      )
      .then(response => {
        this.setState({ pokemon: response.data.data });
      });
  }
  navigateBack = () => {
    this.props.history.goBack();
  };
  render() {
    return (
      <Box>
        <Row width="100%" py={6}>
          <Box
            flex={3}
            textAlign="center"
            fontSize={68}
            fontWeight="700"
            color="white"
          >
            <ArrowButton
              overrideCSS="flex: 0;float: left;
              margin-top: 22px; margin-left: 20px; position: absolute;"
              overrideButtonCSS="background-color: white; color: #4e988f"
              isLeft
              isActive
              callbackFunc={this.navigateBack}
            />
            {this.state.pokemon.name}
          </Box>
        </Row>
        <Box bg="white" borderRadius={3} mx={[4, 10, 32, 40]} px={8} py={3}>
          <Row borderBottom="1px solid #e0e0e0" pb={1}>
            <Box fontWeight={700} alignSelf="center">
              {this.state.pokemon.name}
            </Box>
            <Box fontWeight={700} alignSelf="center" color="#808080ba" ml={4}>
              #{this.state.pokemon.id}
            </Box>

            <Row flex={1} justifyContent="flex-end !important">
              {this.state.pokemon.types &&
                this.state.pokemon.types.map(type => (
                  <TypeDisplay key={this.state.pokemon.name + type}>
                    {type}
                  </TypeDisplay>
                ))}
            </Row>
          </Row>
          <Row my={4}>
            <Row flex={1} alignItems="center">
              <img src={this.state.pokemon.image} />
            </Row>
            <Box flex={2}>
              {this.state.pokemon.stats &&
                Object.entries(this.state.pokemon.stats).map(stat => (
                  <StatBar key={stat[0]} stat={stat}></StatBar>
                ))}
            </Box>
          </Row>
          <Row justifyContent="flex-start !important" fontWeight={700}>
            {this.state.pokemon.genus}
          </Row>
          <Row>
            <p>{this.state.pokemon.description}</p>
          </Row>
          <Row my={4}>
            <DetailSectionHeader>Profile</DetailSectionHeader>
          </Row>
          <Box mx={10}>
            <Row>
              <Box flex={1} fontWeight={700}>
                Height:
              </Box>
              <Box flex={1}>{this.state.pokemon.height} m</Box>
              <Box flex={1} fontWeight={700}>
                Weight:
              </Box>
              <Box flex={1}>{this.state.pokemon.weight} kg</Box>
            </Row>
            <Row>
              <Box flex={1} fontWeight={700}>
                Egg Groups:
              </Box>
              <Box flex={1}>
                {this.state.pokemon.egg_groups &&
                  this.state.pokemon.egg_groups.join(", ")}
              </Box>
              <Box flex={1} fontWeight={700}>
                Abilities:
              </Box>
              <Box flex={1}>
                {this.state.pokemon.abilities &&
                  this.state.pokemon.abilities.join(", ")}
              </Box>
            </Row>
          </Box>
        </Box>
      </Box>
    );
  }
}

export default withRouter(PokemonDetail);
