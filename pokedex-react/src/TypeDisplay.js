import React from "react";
import { Box } from "@chakra-ui/core";

class TypeDisplay extends React.Component {
  render() {
    var typeColor = getTypeColor(this.props.children);
    return (
      <Box
        bg={typeColor.primaryColor}
        border="1px solid"
        borderColor={typeColor.secondaryColor}
        color={typeColor.secondaryColor}
        rounded="5px"
        px="5px"
        m="2"
      >
        {this.props.children}
      </Box>
    );
  }
}

function getTypeColor(type) {
  switch (type) {
    case "bug":
      return { primaryColor: "green.300", secondaryColor: "green.800" };
    case "dragon":
      return { primaryColor: "purple.700", secondaryColor: "purple.50" };
    case "electric":
      return { primaryColor: "yellow.200", secondaryColor: "yellow.500" };
    case "fighting":
      return { primaryColor: "orange.700", secondaryColor: "orange.50" };
    case "fire":
      return { primaryColor: "red.500", secondaryColor: "red.900" };
    case "flying":
      return { primaryColor: "teal.300", secondaryColor: "teal.800" };
    case "ghost":
      return { primaryColor: "purple.300", secondaryColor: "purple.900" };
    case "grass":
      return { primaryColor: "green.100", secondaryColor: "green.800" };
    case "ground":
      return { primaryColor: "yellow.600", secondaryColor: "yellow.50" };
    case "ice":
      return { primaryColor: "cyan.100", secondaryColor: "cyan.800" };
    case "normal":
      return { primaryColor: "orange.100", secondaryColor: "orange.800" };
    case "poison":
      return { primaryColor: "purple.100", secondaryColor: "purple.800" };
    case "psychic":
      return { primaryColor: "pink.500", secondaryColor: "pink.900" };
    case "rock":
      return { primaryColor: "yellow.600", secondaryColor: "yellow.900" };
    case "water":
      return { primaryColor: "blue.200", secondaryColor: "blue.900" };
    case "fairy":
      return { primaryColor: "pink.300", secondaryColor: "pink.800" };
    case "dark":
      return { primaryColor: "red.900", secondaryColor: "red.50" };
    case "steel":
      return { primaryColor: "gray.600", secondaryColor: "gray.50" };
    default:
      return { primaryColor: "gray.600", secondaryColor: "gray.50" };
  }
}

export default TypeDisplay;
