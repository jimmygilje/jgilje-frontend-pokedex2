/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import styled from "@emotion/styled";
import { FaArrowLeft, FaArrowRight } from "react-icons/fa";

const ArrowBtn = styled.p`
  background-color: #4e988f;
  border-radius: 50%;
  padding: 20px;
  display: flex;
  align-items: center;
  color: #fff;
  font-size: 18px;
  border: none;
  outline: none;
`;

const inActiveCSS = "filter: opacity(0.5)";

const ArrowButton = props => (
  <div
    css={css`
      display: flex;
      align-items: center;
      flex: 1;
      justify-content: center;
      ${props.overrideCSS}
    `}
  >
    <ArrowBtn
      as="button"
      disabled={!props.isActive}
      css={css`
        ${!props.isActive && inActiveCSS}
        ${props.overrideButtonCSS}
      `}
      onClick={props.callbackFunc}
    >
      {props.isLeft ? <FaArrowLeft /> : <FaArrowRight />}
    </ArrowBtn>
  </div>
);

export default ArrowButton;
