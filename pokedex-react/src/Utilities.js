/** @jsx jsx */
import { css } from "@emotion/core";
import styled from "@emotion/styled";
import {
  typography,
  space,
  color,
  flexbox,
  layout,
  border
} from "styled-system";

//Using Emotion and Styled system to learn more about them
export const Box = styled("div")(
  css`
    text-align: left;
  `,
  typography,
  space,
  color,
  flexbox,
  layout,
  border
);

export const Row = styled(Box)`
  display: flex;
  justify-content: center;
`;

export const Column = styled(Box)`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;
