import React from "react";
import { Box, Flex, Image } from "@chakra-ui/core";
import TypeDisplay from "./TypeDisplay";
import { Link } from "react-router-dom";

class PokemonCard extends React.Component {
  render() {
    return (
      <Link to={`/detail/${this.props.pokemon.id}`}>
        <Box bg="white" p={4} height="100%">
          <Flex justifyContent="flex-start">{this.props.pokemon.name}</Flex>
          <Flex justifyContent="center">
            <Image src={this.props.pokemon.image} />
          </Flex>
          <Flex justifyContent="flex-end">
            {this.props.pokemon.types.map(type => (
              <TypeDisplay key={this.props.pokemon.name + type}>
                {type}
              </TypeDisplay>
            ))}
          </Flex>
        </Box>
      </Link>
    );
  }
}

export default PokemonCard;
